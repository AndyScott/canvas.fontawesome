using System;
using System.Web.UI.WebControls;
using System.Collections.Generic;

using Net4orce.Core.Canvas.Meta;

namespace Canvas.FontAwesome
{
  public class IncludeMe : ControlBase, IDesignerControl
  {
    protected override void initialize()
    {
      base.initialize();
    }
    public bool Designer { get; private set; }
  }
}