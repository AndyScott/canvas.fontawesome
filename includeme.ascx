<%@ Assembly src="/__tmp__/bin/Canvas.FontAwesome.dll" %>
<%@ Control Language="C#" AutoEventWireup="true" Inherits="Canvas.FontAwesome.IncludeMe" %>
<%@ Import namespace="Canvas.FontAwesome"%>

<% /*
  To include Font Awesome in your Canvas project include 
  this control in any section & the less CSS will be compiled
  into the project.
  
  Doesn't work use Admin - LessCSS & add : 
  
  @import "../../__dynamics__/Canvas.FontAwesome/less/font-awesome.less";

*/ %>

<i class="fa fa-camera-retro fa-lg"></i> fa-lg
<i class="fa fa-camera-retro fa-2x"></i> fa-2x
<i class="fa fa-camera-retro fa-3x"></i> fa-3x
<i class="fa fa-camera-retro fa-4x"></i> fa-4x
<i class="fa fa-camera-retro fa-5x"></i> fa-5x