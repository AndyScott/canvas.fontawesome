# Canvas Font Awesome Support - Readme

Using Canvas's Dynamic Assemblies, Add a new assembly & use this git repo as the source URL.  

Add an `@import` statement to Admin -> LessCSS & save.  EG: 
    
    @import "../../__dynamics__/Canvas.FontAwesome/less/font-awesome.less";

That's it, you now have font-awesome support & you can test it using the following example : 
    
    <i class="fa fa-camera-retro"></i>

## Canvas support for FontAwesome

* This repo allows you to quickly include Font Awesome & access all source codes for tailored usage.
* 0.01